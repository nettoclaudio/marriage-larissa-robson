'use strict';

const redis = require('redis');

function isProduction() {
  return process.env.APP_ENVIRONMENT === 'production';
}

function createRedisClient() {
  const hostname = process.env.CACHE_HOSTNAME;
  const port     = process.env.CACHE_PORT     || 6379;
  const database = process.env.CACHE_DATABASE || 0;
  const password = process.env.CACHE_PASSWORD || '';
  const prefix   = process.env.CACHE_PREFIX   || 'lr:';

  const redisConfigurations = {
    host:     hostname,
    port:     port,
    password: password,
    db:       database,
    prefix:   prefix
  };

  return redis.createClient(redisConfigurations);
}

function sendEmail(message) {
  const sendgridMail = require('@sendgrid/mail');
  
  sendgridMail.setApiKey(process.env.SENDGRID_API_KEY);
  sendgridMail.setSubstitutionWrappers('{{', '}}');

  sendgridMail.send(message);
}

const redisClient = createRedisClient();

redisClient.on("message", function(channel, message) {
  console.log(message);
  
  const payload = JSON.parse(message);

  console.log(payload);

  const willBePresent = payload['will-be-present']; 

  const sendgridTemplate = willBePresent === "true" ? "2f84f623-d664-4c00-8a02-608a226165d7" : "22b1ff70-c7a2-40fe-8037-531279a6ec2c";

  const sengridBCCs = [];

  const emailsBCC = process.env.EMAIL_BCC.split(",");

  for (let index=0; index < emailsBCC.length; index++) {
    sengridBCCs.push(emailsBCC[index]);
  }

  const email = {
    to: payload['email'],
    from: "www.larissaerobson.com.br <nao-responda@larissaerobson.com.br>",
    bcc: sengridBCCs,
    subject: "[L&R] Confirmação/Cancelamento de presença",
    html: ' ',
    templateId: sendgridTemplate,
    substitutions: {
      firstname:   payload['fullname'].substring(0, payload['fullname'].indexOf(" ")),
      fullname:    payload['fullname'],
      email:       payload['email'],
      cellphone:   payload['cellphone'] || '-',
      children:    payload['children']  || 0,
    }
  };

  if (isProduction()) {
    sendEmail(email);
    
    console.log(email);
  } else {
    console.log(email);
  }
});

redisClient.subscribe(process.env.CACHE_CHANNEL_CONFIRMATION);