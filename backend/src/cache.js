'use strict';

const redis = require('redis');

class CacheClient {

    constructor(hostname, port, password='', database=0, prefix=null) {
        this.client = redis.createClient({
            password: password,
            host: hostname,
            port: port,
            db: database,
            prefix: prefix,
        });
    }

    storeGuest(guest, remoteIP, timestamp=new Date()) {
        const payload = {
            'guest': guest,
            'remote-ip': remoteIP,
            'timestamp': timestamp,
        }

        console.log(`New guest has been confirmed presence(or not): ${JSON.stringify(payload)}`);

        this.client.rpush('invited-clients', JSON.stringify(payload));
        this.client.publish(process.env.CACHE_CHANNEL_CONFIRMATION, JSON.stringify(guest));
    }

    storeBadClient(fullname, remoteIP, timestamp=new Date()) {
        const payload = {
            'fullname':  fullname,
            'remote-ip': remoteIP,
            'timestamp': timestamp,
        }

        console.log(`New bad guy in the dark list: ${JSON.stringify(payload)}`);

        this.client.rpush('bad-clients', JSON.stringify(payload));
    }

    listBadClients() {
        this.client.lrange('bad-clients', 0, -1, function(error, payload) {
            if (error != null) {
                console.log(error);

                return;
            }
        
            payload.forEach(function(element) {
                console.log(element);
            }, this);
        });
    }

    quit() {
        this.client.quit();
    }
}

module.exports = CacheClient;