'use strict';

const express = require('express');
const recaptcha2 = require('recaptcha2');

const CacheClient = require('./cache.js');

const Server = {
    SERVER_PORT: process.env.SERVER_PORT || 3000,

    GRECAPTCHA_SITE_KEY: process.env.GRECAPTCHA_SITE_KEY,
    GRECAPTCHA_PRIVATE_KEY: process.env.GRECAPTCHA_PRIVATE_KEY,
    
    CACHE_HOSTNAME: process.env.CACHE_HOSTNAME,
    CACHE_PORT: process.env.CACHE_PORT || 6379,
    CACHE_DATABASE: process.env.CACHE_DATABASE || 0,
    CACHE_PASSWORD: process.env.CACHE_PASSWORD || '',
    CACHE_PREFIX: process.env.CACHE_PREFIX || 'lr:',

    registerServerConfigs: function(app) {
        var bodyParser = require('body-parser');
        var cors = require('cors');

        app.use(cors());
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
    },

    registerRoutes: function(app) {
        app.get('/v1/status', function(request, response) {
            response.sendStatus(200);
        });

        app.post('/v1/confirmation', function(request, response) {
            const payload = request.body;

            console.log(payload);

            const recaptchaResponse = payload["g-recaptcha-response"];
            const remoteIP = request.connection.remoteAddress;

            var recaptcha = new recaptcha2({
                siteKey:   Server.GRECAPTCHA_SITE_KEY,
                secretKey: Server.GRECAPTCHA_PRIVATE_KEY
            });

            recaptcha.validate(recaptchaResponse, remoteIP).then(function() {
                const fullname      = payload["fullname"];
                const email         = payload["email"];
                const children      = parseInt(payload["children"]);
                const willBePresent = payload["will-be-present"];
                const cellphone     = payload["cellphone"];

                if (!fullname || !email || !willBePresent || children === "" || children == null) {
                    response.json({ "success": false }).status(400).send();
                    return;
                }

                const guest = {
                    "fullname":        fullname,
                    "email":           email,
                    "children":        children,
                    "will-be-present": willBePresent,
                    "cellphone":       cellphone,
                }

                Server.database.storeGuest(guest, remoteIP, new Date());

                response.json({ "success": true }).status(201).send();
            }).catch(function(errorCodes) {
                Server.database.storeBadClient(payload["fullname"], remoteIP, new Date());

                console.log(`Google Recaptcha errors: ${recaptcha.translateErrors(errorCodes)}`);
                
                response.json({ "success": false }).status(403).send();
            });
        });
    
        app.use("*", function(request, response) {
            response.json({ "error": 404 }).status(404).send();
        })
    },

    run: function() {
        var app = express();

        Server.registerServerConfigs(app);
        Server.registerRoutes(app);

        Server.database = new CacheClient(Server.CACHE_HOSTNAME, Server.CACHE_PORT, Server.CACHE_PASSWORD, Server.CACHE_DATABASE, Server.CACHE_PREFIX);

        app.listen(Server.SERVER_PORT);
        
        console.log('Server is running on port: ' + Server.SERVER_PORT);
    }
};

Server.run();