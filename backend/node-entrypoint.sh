#!/bin/sh
set -e

function testVariables {
    if [ -z "${APP_ENVIRONMENT}" ]; then
        printf "%s" "The APP_ENVIRONMENT variable is not set. Assuming development environment." > /dev/stderr
        
        export APP_ENVIRONMENT='development'
    fi

    if [ -z "${APP_DIR}" ]; then
        printf "%s" "The APP_DIR variable is not set. Assuming default value(/mnt/app)." > /dev/stderr

        export APP_DIR='/mnt/app'
    fi

    if [[ "${APP_ENVIRONMENT}" = 'production' ]]; then
        if [ -z "${APP_REMOTE_REPOSITORY}" ]; then
            printf "%s" "The APP_REMOTE_REPOSITORY variable is not set. Aborting..." > /dev/stderr

            exit 1
        fi

        if [ -z "${APP_REMOTE_REPOSITORY_DESTINATION}" ]; then
            printf "%s" "The APP_REMOTE_REPOSITORY_DESTINATION variable is not set. Aborting..." > /dev/stderr

            exit 1
        fi
    fi
}

function checkoutProject {
    local remoteRepository=${1}
    local destinationDir=${2}

    git clone -b master ${remoteRepository} ${destinationDir}
}

function main {
    testVariables

    if [[ "${APP_ENVIRONMENT}" = 'production' ]]; then
        checkoutProject ${APP_REMOTE_REPOSITORY} ${APP_REMOTE_REPOSITORY_DESTINATION}
    fi

    cd ${APP_DIR}

    npm install && exec npm start
}

main