'use strict';

function initializeMapLocations() {
  const locationParty = {
    lat: -22.906131,
    lng: -43.257702
  };

  const mapParty = new google.maps.Map(document.getElementById('marriage-party-location'), {
    center: locationParty,
    zoom:   14
  });

  new google.maps.Marker({
    icon:     '/assets/icons/heart-64x64.png',
    position: locationParty,
    map:      mapParty,
    title:   'Local da festa'
  });

  const locationChurch = {
    lat: -22.895673,
    lng: -43.236793
  };

  const mapChurch = new google.maps.Map(document.getElementById('map-location-church'), {
    center: locationChurch,
    zoom:   14
  });

  new google.maps.Marker({
    icon:     '/assets/icons/heart-64x64.png',
    position: locationChurch,
    map:      mapChurch,
    title:   'Local da cerimônia religiosa'
  });
}