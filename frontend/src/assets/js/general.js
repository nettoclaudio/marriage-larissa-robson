'use strict';

var LRApp = {
    MARRIAGE_DATE: new Date("2017-12-16T19:00:00.000Z"),

    convertMilisecondsToDays: function(miliseconds) {
        let oneDayInMiliseconds = 24 * 60 * 60 * 1000;

        return Math.floor(miliseconds / oneDayInMiliseconds);
    },

    getHashFromURL: function() {
        return window.location.hash;
    },

    hasHashOnURL: function() {
        if (LRApp.getHashFromURL()) {
            return true;
        } else {
            return false;
        }
    },
};

LRApp.Header = {
    calculateRemainingDaysUntilMarriageDate: function() {
        var currentDate = new Date();

        var remainingDaysInMiliseconds = LRApp.MARRIAGE_DATE - currentDate;

        return LRApp.convertMilisecondsToDays(remainingDaysInMiliseconds);
    },

    getCountdownMarriageMessage: function() {
        var remainingDays = LRApp.Header.calculateRemainingDaysUntilMarriageDate()

        if (remainingDays == 1) {
          return "Falta um dia.";
        }
        
        if (remainingDays > 1) {
          return "Faltam " + remainingDays + " dias.";
        }
      
        if (remainingDays == 0) {
          return "É hoje!!!";
        }
      
        if (remainingDays < 0 ) {
          return "Recém casados.";
        }
    },

    load: function() {
        $('#remaining-days').text(LRApp.Header.getCountdownMarriageMessage());
    }
};

LRApp.Menu = {
    clickOnNavigationMenuByHashWhenHashExistsOnURL: function() {
        if (LRApp.hasHashOnURL()) {
            var anchorSelectorByHash = 'ul.nav > li > a[href="' + LRApp.getHashFromURL() + '"]';
            
            $(anchorSelectorByHash).click();
        }
    },

    clickOnNavigationMenu: function() {
        var oldActiveAnchor = $(this).parent().siblings().filter(function(index) {
            return $(this).hasClass("active");
        }).children()[0];

        $($(oldActiveAnchor).attr('href')).slideUp();

        $(oldActiveAnchor).parent().removeClass("active");
        
        $(this).parent().addClass("active");

        $($(this).attr('href')).show("slow");
    },

    load: function() {
        $('ul.nav > li > a').click(LRApp.Menu.clickOnNavigationMenu);

        LRApp.Menu.clickOnNavigationMenuByHashWhenHashExistsOnURL();
    },
};

LRApp.Confirmation = {
    sendConfirmationForm: function(event) {
        event.preventDefault();

        const fullname          = $('input[name="fullname"]', '#form-confirmation').val();
        const email             = $('input[name="email"]', '#form-confirmation').val();
        const children          = $('input[name="children"]', '#form-confirmation').val();
        const cellphone         = $('input[name="cellphone"]', '#form-confirmation').val();
        const willBePresent     = $('input[name="confirmation-option"]:checked', '#form-confirmation').val() === "y" ? true : false;
        const recaptchaResponse = $('#g-recaptcha-response', '#form-confirmation').val();
        
        const payload = {
            "fullname": fullname,
            "email": email,
            "children": children,
            "will-be-present": willBePresent,
            "cellphone": cellphone,
            "g-recaptcha-response": recaptchaResponse,
        };
        
        $.ajax({
            url: "http://api.larissaerobson.com.br/v1/confirmation",
            type: "POST",
            dataType: "json",
            data: payload,
            crossDomain: true,
        
            beforeSend: function() {
                $('.modal-loading').show();
            },

            complete: function() {
                $('#form-confirmation').each(function() {
                    this.reset();
                });

                $('.modal-loading').hide();
            },

            error: function(response) {
                const errorMessage = `
                <p>Infelizmente não foi possível processar a sua requisição...</p>
                <p>Sentimos muito pelo inconveniente... Tente novamente mais tarde.</p>`;

                $('.modal-title', '#modal-confirmation-status').text("Ops! Alguma coisa deu errado :/");
                $('.modal-body',  '#modal-confirmation-status').html(errorMessage);
                
                $('#modal-confirmation-status').modal('show');
            },
        
            success: function(response) {
                let titleMessage, bodyMessage;

                if (response["success"] === true) {
                    titleMessage = "Obrigado!";

                    bodyMessage = `
                    <p>Sua informações foram recebidas com sucesso!</p>
                    <p>Dentro de instantes, você receberá uma messagem de confirmação no endereço <strong>${email}</strong>.</p>`
                } else {
                    titleMessage = "Alguma coisa deu errado...";

                    bodyMessage = `
                    <p>Não foi possível adicionar a sua confirmação.<p>
                    <p>Certifique-se de preencher corretamente os campos e envie novamente.</p>`
                }

                $('.modal-title', '#modal-confirmation-status').text(titleMessage);
                $('.modal-body',  '#modal-confirmation-status').html(bodyMessage);
                
                $('#modal-confirmation-status').modal('show');
            }
        });
    },

    load: function() {
        $('[data-toggle="tooltip"]').tooltip();

        $('.phone-with-ddd').mask('(00) X0000-0000', {
            clearIfNotMatch: true,
            
            translation: {
                'X': {
                  pattern: /[9]/,
                  fallback: '9'
                },
            }
        });

        $('input[name="confirmation-option"]', '#form-confirmation').click(function() {
            const option = $(this).val();

            if (option === "n") {
                $('#form-group-confirmation-children').slideUp();
                $('input[name=children]').val(0);
            } else {
                $('#form-group-confirmation-children').show("slow");
            }
        });

        $('#form-confirmation').submit(LRApp.Confirmation.sendConfirmationForm);
    }
};

$(document).ready(function() {
    LRApp.Header.load();
    LRApp.Menu.load();
    LRApp.Confirmation.load();
});
